package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import kotlinx.coroutines.flow.Flow

interface NotificationUseCase {

    fun registerNotification(hash: String, payloadRequest: PayloadRequest) : Resource<out Unit>
}