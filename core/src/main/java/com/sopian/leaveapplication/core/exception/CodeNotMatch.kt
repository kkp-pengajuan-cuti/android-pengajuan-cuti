package com.sopian.leaveapplication.core.exception

import java.lang.Exception

class CodeNotMatch constructor(message: String?) : Exception(message)