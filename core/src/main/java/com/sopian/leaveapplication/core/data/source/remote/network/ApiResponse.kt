package com.sopian.leaveapplication.core.data.source.remote.network

import java.lang.Exception

sealed class ApiResponse<out R> {
    data class Success<out T: Any>(val data: T) : ApiResponse<T>()
    data class Error(val errorMessage: String? = null, val exception: Exception? = null) : ApiResponse<Nothing>()
    object Empty : ApiResponse<Nothing>()
}