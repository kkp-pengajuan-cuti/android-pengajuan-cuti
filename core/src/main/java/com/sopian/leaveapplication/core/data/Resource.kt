package com.sopian.leaveapplication.core.data

import java.lang.Exception

sealed class Resource<T: Any>(
    val data: T? = null,
    val message: String? = null,
    val exception: Exception? = null
) {
    class Success<T: Any>(data: T) : Resource<T>(data)
    class Loading<T: Any>(data: T? = null) : Resource<T>(data)
    class Error<T: Any>(message: String? = null, data: T? = null, exception: Exception? = null) : Resource<T>(data, message, exception)
}
