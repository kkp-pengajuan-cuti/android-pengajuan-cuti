package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.VerificationRepository
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class VerificationInteractor @Inject constructor(
    private val verificationRepository: VerificationRepository
) : VerificationUseCase {
    override fun verify(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> {
        return verificationRepository.verify(hash, payloadRequest)
    }
}