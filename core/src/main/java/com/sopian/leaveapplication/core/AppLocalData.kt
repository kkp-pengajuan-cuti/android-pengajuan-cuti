package com.sopian.leaveapplication.core

import android.content.Context
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import timber.log.Timber
import java.util.*
import javax.inject.Inject

/**
 * The idea of App local data is to provide a Key-value local internal
 * file. Mainly to keep access token
 *
 */
class AppLocalData @Inject constructor() {

    var KEY_FILE_PREF = "fi_leave"
    var KEY_ACCESS_TOKEN = "ACC_TOKEN"
    var KEY_REFRESH_TOKEN = "ACC_REFRESH_TOKEN"
    var KEY_DISPLAY = "ACC_DISPLAY"
    var KEY_DEVICEID = "ACC_DEVICEID"
    var KEY_NIP = "ACC_NIP"
    var KEY_PROFILE_IMG = "ACC_PROFILE_IMG"
    var KEY_FCM_TOKEN = "ACC_FCM_TOKEN"
    var KEY_USER_TYPE = "ACC_USER_TYPE"


    val TAG = "APPDATA"


    /**
     * Get generated device Id.
     *
     * As advised in:
     * https://developer.android.com/training/articles/user-data-ids
     *
     *
     * @return GUID
     */
    fun getGeneratedDeviceId(): String {
        return UUID.randomUUID().toString()
    }


    /**
     * Get access token from current key
     * @param context context
     * @return access token, "" if not found
     */
    fun getKeyAccessToken(context: Context): String? {
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        return sharedPref.getString(KEY_ACCESS_TOKEN, "")
    }

    /**
     * Get refresh token from current key
     * @param context context
     * @return refresh token, "" if not found
     */
    fun getKeyRefreshToken(context: Context): String? {
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        return sharedPref.getString(KEY_REFRESH_TOKEN, "")
    }

    /**
     * Write deviceId in SharedPreference
     * @param context context
     * @param deviceId device Id
     */
    fun setKeyDeviceId(context: Context?, deviceId: String?) {
        if (context == null) {
            return
        }
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        sharedPref.edit().apply {
            putString(KEY_DEVICEID, deviceId)
            //We need to make it available RIGHT-AWAY
            //editor.apply();
            apply()
        }
    }

    fun setKeyFcmToken(context: Context?, fcmToken: String?) {
        if (context == null) {
            return
        }
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        sharedPref.edit().apply {
            putString(KEY_FCM_TOKEN, fcmToken)
            //We need to make it available RIGHT-AWAY
            //editor.apply();
            apply()
        }
    }

    /**
     * Get device Id from current shared Preference
     * @param context context
     * @return device Id, "" if not found
     */
    fun getKeyDeviceId(context: Context?): String? {
        if (context != null){
            val sharedPref = context.getSharedPreferences(
                KEY_FILE_PREF, Context.MODE_PRIVATE
            )
            return sharedPref.getString(KEY_DEVICEID, "")
        }else{
            Timber.tag(TAG).d("Can't find deviceId in SharedPreference!! Context is null!")
        }
        return null
    }

    fun getKeyFcmToken(context: Context?): String? {
        if (context != null){
            val sharedPref = context.getSharedPreferences(
                KEY_FILE_PREF, Context.MODE_PRIVATE
            )
            return sharedPref.getString(KEY_FCM_TOKEN, "")
        }else{
            Timber.tag(TAG).d("Can't find fcmToken in SharedPreference!! Context is null!")
        }
        return null
    }

    fun dropKeyDeviceId(context: Context?) {
        if (context != null){
            val sharedPref = context.getSharedPreferences(
                KEY_FILE_PREF, Context.MODE_PRIVATE
            )
            sharedPref.edit().apply {
                remove(KEY_DEVICEID)
                apply()
            }
        }else{
            Timber.tag(TAG).d("Can't remove deviceId in SharedPreference!! Context is null!")
        }
    }

    /**
     * Write user data, will REPLACE if existing key exists
     * @param context context
     * @param user
     */
    fun setUserLoggedIn(context: Context?, user: LoggedInUser) {
        if (context == null) {
            return
        }
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        sharedPref.edit().apply {
            putString(KEY_ACCESS_TOKEN, user.accessToken)
            putString(KEY_REFRESH_TOKEN, user.refreshToken)
            putString(KEY_DISPLAY, user.displayName)
            putInt(KEY_NIP, user.nip)
            putString(KEY_PROFILE_IMG, user.profilePic)
            putString(KEY_USER_TYPE, user.userType)
            //We need to make it available RIGHT-AWAY
            //apply();
            apply()
        }
    }


    /**
     * Drop user data
     * @param context context
     */
    fun dropUserLoggedIn(context: Context?) {
        if (context == null) {
            return
        }
        val sharedPref = context.getSharedPreferences(
            KEY_FILE_PREF, Context.MODE_PRIVATE
        )
        sharedPref.edit().apply {
            remove(KEY_ACCESS_TOKEN)
            remove(KEY_REFRESH_TOKEN)
//            remove(KEY_DISPLAY)
            remove(KEY_NIP)
            remove(KEY_PROFILE_IMG)
            remove(KEY_USER_TYPE)
            //We need to make it available RIGHT-AWAY
            //apply();
            apply()
        }

    }


    /**
     * Get user data. All required data must exists and NOT 'corrupt',
     * missing any data will result to NULL
     *
     * @param context context
     * @return user
     */
    fun getUserLoggedIn(context: Context?): LoggedInUser? {
        if (context != null) {
            Timber.d("Finding user in SharedPreference")
            val sharedPref = context.getSharedPreferences(
                KEY_FILE_PREF, Context.MODE_PRIVATE
            )
            val accessToken = sharedPref.getString(KEY_ACCESS_TOKEN, "")
            val refreshToken = sharedPref.getString(KEY_REFRESH_TOKEN, "")
            val nip = sharedPref.getInt(KEY_NIP, 1)
            val displayName = sharedPref.getString(KEY_DISPLAY, "")
            val profilePic = sharedPref.getString(KEY_PROFILE_IMG, "")
            val userTYpe = sharedPref.getString(KEY_USER_TYPE, "")

            if (!accessToken!!.isEmpty() && !displayName!!.isEmpty()) {
                val user = LoggedInUser(nip, displayName, accessToken, refreshToken!!, profilePic!!, userTYpe!!)
                user.nip = nip
                user.displayName = displayName
                user.accessToken = accessToken
                user.refreshToken = refreshToken
                user.profilePic = profilePic
                user.userType = userTYpe
                return user
            }
        } else {
            Timber.tag(TAG).d("Can't find user in SharedPreference!! Context is null!")
        }
        return null
    }
}