package com.sopian.leaveapplication.core.data.source

import com.sopian.leaveapplication.core.data.NetworkBoundResource
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.RemoteDataSource
import com.sopian.leaveapplication.core.data.source.remote.network.ApiResponse
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.data.source.remote.response.LeaveResponse
import com.sopian.leaveapplication.core.domain.model.Leave
import com.sopian.leaveapplication.core.domain.repository.ILeaveRepository
import com.sopian.leaveapplication.core.utils.DataMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LeaveRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : ILeaveRepository {

    override fun getLeaves(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out List<Leave>>> =
        object : NetworkBoundResource<List<Leave>, List<LeaveResponse>>() {
            override fun load(data: List<LeaveResponse>?): Flow<List<Leave>> {
                return flow {
                    emit(DataMapper.mapLeaveResponseToDomain(data))
                }
            }

            override suspend fun createCall(): Flow<ApiResponse<List<LeaveResponse>?>> =
                remoteDataSource.getLeave(hash, payloadRequest)

        }.asFlow()

    override fun approve(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        object : NetworkBoundResource<Unit, Unit>() {
            override fun load(data: Unit?): Flow<Unit> {
                return flow { emit(Unit) }
            }

            override suspend fun createCall(): Flow<ApiResponse<Unit?>> =
                remoteDataSource.approve(hash, payloadRequest)
        }.asFlow()

    override fun reject(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        object : NetworkBoundResource<Unit, Unit>() {
            override fun load(data: Unit?): Flow<Unit> {
                return flow { emit(Unit) }
            }

            override suspend fun createCall(): Flow<ApiResponse<Unit?>> =
                remoteDataSource.reject(hash, payloadRequest)
        }.asFlow()

    override fun createLeave(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        object : NetworkBoundResource<Unit, Unit>() {
            override fun load(data: Unit?): Flow<Unit> {
                return flow { emit(Unit) }
            }

            override suspend fun createCall(): Flow<ApiResponse<Unit?>> =
                remoteDataSource.createLeave(hash, payloadRequest)
        }.asFlow()
}