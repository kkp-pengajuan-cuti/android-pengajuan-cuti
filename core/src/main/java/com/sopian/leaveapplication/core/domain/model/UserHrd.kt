package com.sopian.leaveapplication.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserHrd (
    val id: Int,
    val name: String
) : Parcelable