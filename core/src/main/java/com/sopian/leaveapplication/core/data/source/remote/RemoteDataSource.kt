package com.sopian.leaveapplication.core.data.source.remote

import android.util.Log
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.data.source.remote.network.ApiResponse
import com.sopian.leaveapplication.core.data.source.remote.network.ApiService
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.data.source.remote.response.LeaveResponse
import com.sopian.leaveapplication.core.data.source.remote.response.TokenResponse
import com.sopian.leaveapplication.core.data.source.remote.response.UserHrdResponse
import com.sopian.leaveapplication.core.exception.AccessNotAuthorized
import com.sopian.leaveapplication.core.exception.CodeExpired
import com.sopian.leaveapplication.core.exception.CodeNotMatch
import com.sopian.leaveapplication.core.exception.NipNotFound
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSource @Inject constructor(private val apiService: ApiService) {

    companion object {
        const val TAG = "RemoteDataSource"
    }

//    suspend fun getTokenAndUser(params: HashMap<String?, Any?>): Flow<ApiResponse<TokenResponse?>> {
//        return flow {
//            try {
//                val response = apiService.getToken(params)
//                if (response.isSuccessful && response.body()?.data != null) {
//                    emit(ApiResponse.Success(response.body()?.data))
//                } else if (!response.isSuccessful) {
//                    if (response.code() == 401) {
//                        // We're revoking the access token
//                        emit(ApiResponse.Error(null, AccessNotAuthorized("User not authorized")))
//                    }
//                }
//            } catch (e : Exception) {
//                Timber.tag(TAG).e(e.toString())
//                emit(ApiResponse.Error(e.toString()))
//            }
//        }.flowOn(Dispatchers.IO)
//    }

    suspend fun getTokenAndUser(hash: String, tokenRequest: TokenRequest): Flow<ApiResponse<TokenResponse>> {
        return flow {
            try {
                val response = apiService.getToken(hash, tokenRequest)
                emit(ApiResponse.Success(response.data))
            } catch (e : Exception) {
                Timber.tag(TAG).e(e.toString())
                emit(ApiResponse.Error(e.toString(), null))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getLeave(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<List<LeaveResponse>>> {
        return flow {
            try {
                val response = apiService.getLeave(hash, payloadRequest)
                val dataArray = response.body()!!.data
                if (response.code() == 401) {
                    emit(ApiResponse.Error("Unauthenticated", AccessNotAuthorized("Unauthenticated")))
                }
                if (dataArray.isNotEmpty()){
                    emit(ApiResponse.Success(dataArray))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.toString())
                emit(ApiResponse.Error(e.toString(), null))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getHrd(): Flow<ApiResponse<List<UserHrdResponse>>> {
        return flow {
            try {
                val response = apiService.getHrd()
                val dataArray = response.body()!!.data
                if (dataArray.isNotEmpty()){
                    emit(ApiResponse.Success(dataArray))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.toString())
                emit(ApiResponse.Error(e.toString(), null))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun register(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<Unit>> {
        return flow {
            try {
                val response = apiService.register(hash, payloadRequest)
                if (response.code() == 404) {
                    emit(ApiResponse.Error(null, NipNotFound("Nip not found")))
                }
                emit(ApiResponse.Success(Unit))
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.toString())
                emit(ApiResponse.Error(e.toString(), null))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun verify(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<Unit>> {
        return flow {
            try {
                val response = apiService.verify(hash, payloadRequest)
                when (response.code()) {
                    400 -> emit(ApiResponse.Error(response.body()?.message,
                        CodeExpired(response.body()?.message)))
                    401 -> emit(ApiResponse.Error(response.body()?.message,
                        CodeNotMatch(response.body()?.message)))
                }
                emit(ApiResponse.Success(Unit))
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.toString())
                emit(ApiResponse.Error(e.toString(), null))
            }
        }.flowOn(Dispatchers.IO)
    }

    fun registerNotification(hash: String, payloadRequest: PayloadRequest): Resource<out Unit> {

        try {
            val callSyncPN: Call<Unit> =
                apiService.registerNotification(hash, payloadRequest)
            callSyncPN.enqueue(object : Callback<Unit?> {
                override fun onResponse(
                    call: Call<Unit?>,
                    response: Response<Unit?>
                ) {
                    if (response.isSuccessful) {
                        Timber.tag(TAG).i("Success register Push Notification to server")
                        return
                    }
                }

                override fun onFailure(call: Call<Unit?>, throwable: Throwable) {
                    Timber.tag(TAG).e("Error while register Push Notification to server")
                }
            })
        } catch (e: Exception) {
            Timber.tag(TAG).e(e.toString())
            return Resource.Error(
                exception = IOException("Error logging in. Message: " + e.message, e)
            )
        }

        return Resource.Success(Unit)
    }

    suspend fun approve(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<Unit>> {
        return flow {
            try {
                val response = apiService.approve(hash, payloadRequest)
                if (response.code() == 401) {
                    emit(ApiResponse.Error("Unauthenticated",
                        AccessNotAuthorized("Unauthenticated")))
                }
                emit(ApiResponse.Success(Unit))
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.message.toString())
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun reject(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<Unit>> {
        return flow {
            try {
                val response = apiService.reject(hash, payloadRequest)
                if (response.code() == 401) {
                    emit(ApiResponse.Error("Unauthenticated",
                        AccessNotAuthorized("Unauthenticated")))
                }
                emit(ApiResponse.Success(Unit))
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.message.toString())
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createLeave(hash: String, payloadRequest: PayloadRequest): Flow<ApiResponse<Unit>> {
        return flow {
            try {
                val response = apiService.createLeave(hash, payloadRequest)
                if (response.code() == 401) {
                    emit(ApiResponse.Error("Unauthenticated",
                        AccessNotAuthorized("Unauthenticated")))
                }
                emit(ApiResponse.Success(Unit))
            } catch (e: Exception) {
                Timber.tag(TAG).e(e.message.toString())
                emit(ApiResponse.Error(e.message.toString()))
            }
        }.flowOn(Dispatchers.IO)
    }
}