package com.sopian.leaveapplication.core.exception

import java.lang.Exception

/**
 * Error specific that shows not nip Found
 *
 */
class NipNotFound
/**
 * Constructor
 * @param message a message
 */
constructor (message: String?) : Exception(message)
