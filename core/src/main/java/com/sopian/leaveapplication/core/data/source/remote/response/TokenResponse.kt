package com.sopian.leaveapplication.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(

    @field:SerializedName("user")
    val user: UserResponse,

    @field:SerializedName("access_token")
    val accesToken: String,

    @field:SerializedName("refresh_token")
    val refreshToken: String
)