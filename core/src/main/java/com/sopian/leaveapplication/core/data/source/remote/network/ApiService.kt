package com.sopian.leaveapplication.core.data.source.remote.network

import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.data.source.remote.response.CommonResponse
import com.sopian.leaveapplication.core.data.source.remote.response.LeaveResponse
import com.sopian.leaveapplication.core.data.source.remote.response.TokenResponse
import com.sopian.leaveapplication.core.data.source.remote.response.UserHrdResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @POST("oauth/token")
    suspend fun getToken(
        @Header("hash") hash: String,
        @Body tokenRequest: TokenRequest,
    ) : CommonResponse<TokenResponse>

    @POST("user/register")
    suspend fun register(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<Unit>

    @POST("verification/verify")
    suspend fun verify(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<CommonResponse<Unit>>

    @POST("notification/register")
    fun registerNotification(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Call<Unit>

    @GET("leave")
    suspend fun getLeave(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<CommonResponse<List<LeaveResponse>>>

    @GET("hrd")
    suspend fun getHrd() : Response<CommonResponse<List<UserHrdResponse>>>

    @POST("leave/approve")
    suspend fun approve(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<Unit>

    @POST("leave/reject")
    suspend fun reject(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<Unit>

    @POST("leave/create")
    suspend fun createLeave(
        @Header("hash") hash: String,
        @Body payloadRequest: PayloadRequest
    ) : Response<Unit>
}