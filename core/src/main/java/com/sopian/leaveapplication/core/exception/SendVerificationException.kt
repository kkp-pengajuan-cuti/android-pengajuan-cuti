package com.sopian.leaveapplication.core.exception

import java.lang.Exception

/**
 * Send verification Exception
 */
class SendVerificationException
/**
 * Constructor
 * @param message error message
 */
    constructor (message: String?) : Exception(message)