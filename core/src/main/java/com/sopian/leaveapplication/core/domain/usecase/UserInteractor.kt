package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.UserRepository
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.domain.model.UserHrd
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserInteractor @Inject constructor(
    private val userRepository: UserRepository
) : UserUseCase{

    override fun register(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        userRepository.register(hash, payloadRequest)

    override fun getHrd(): Flow<Resource<out List<UserHrd>>> =
        userRepository.getHrd()
}