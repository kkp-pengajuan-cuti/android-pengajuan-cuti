package com.sopian.leaveapplication.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class EmptyDataResponse(
    @field:SerializedName("success")
    val success: Boolean,

    @field:SerializedName("message")
    val message: String,
)
