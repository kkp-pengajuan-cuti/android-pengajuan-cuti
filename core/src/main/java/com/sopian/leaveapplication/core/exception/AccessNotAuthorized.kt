package com.sopian.leaveapplication.core.exception

import java.lang.Exception

/**
 * Access not authorized
 */
class AccessNotAuthorized
/**
 * Constructor
 * @param message message
 */
constructor (message: String?) : Exception(message)
