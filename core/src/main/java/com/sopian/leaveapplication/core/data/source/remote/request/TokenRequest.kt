package com.sopian.leaveapplication.core.data.source.remote.request

import com.google.gson.annotations.SerializedName

data class TokenRequest(

    @field:SerializedName("grant_type")
    val grantType: String,

    @field:SerializedName("client_id")
    val clientId: Int,

    @field:SerializedName("client_secret")
    val clientSecret: String,

    @field:SerializedName("username")
    val username: String,

    @field:SerializedName("password")
    val password: String,

    @field:SerializedName("scope")
    val scope: String,

    @field:SerializedName("nip")
    val nip: Int,

    @field:SerializedName("device_id")
    val deviceId: String,
)
