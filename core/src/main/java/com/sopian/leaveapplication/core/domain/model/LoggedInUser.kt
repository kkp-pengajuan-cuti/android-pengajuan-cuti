package com.sopian.leaveapplication.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LoggedInUser(
    var nip: Int,
    var displayName: String,
    var accessToken: String,
    var refreshToken: String,
    var profilePic: String,
    var userType: String
) : Parcelable
