package com.sopian.leaveapplication.core.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.lang.StringBuilder
import java.security.InvalidKeyException
import java.security.Key
import java.security.NoSuchAlgorithmException
import java.security.SignatureException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class Sha {

    companion object {
        const val KEY = "!mysecretkey#9^5usdk39d&dlf)03sL"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun hashEncrypt(text: String): String {
        val key = "!mysecretkey#9^5usdk39d&dlf)03sL"
        val iv = "Cfq84/46Qjet3EEQ1HUwSg=="

        val secretKeySpec = SecretKeySpec(key.toByteArray(), "HmacSHA256")

        val ivParameterSpec = IvParameterSpec(Base64.getDecoder().decode(iv))
        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec)
        return Base64.getEncoder().encodeToString(cipher.doFinal(text.toByteArray()))
    }
    fun hash_hmac(): String {
        val str = "{\"nip\":1811511490}!mysecretkey#9^5usdk39d&dlf)03sL"
        val key = "!mysecretkey#9^5usdk39d&dlf)03sL"

        val sha256 = Mac.getInstance("HmacSHA256")
        val secretKey =
            SecretKeySpec(key.toByteArray(charset("UTF-8")), "HmacSHA256")
        sha256.init(secretKey)
        return sha256.doFinal(str.toByteArray()).toString()
    }

    private val HASH_ALGORITHM = "HmacSHA256"

    @Throws(SignatureException::class)
    fun hashMac(text: String, secretKey: String): String? {
        return try {
            val sk: Key = SecretKeySpec(secretKey.toByteArray(), HASH_ALGORITHM)
            val mac = Mac.getInstance(sk.algorithm)
            mac.init(sk)
            val hmac = mac.doFinal(text.toByteArray())
            toHexString(hmac)
        } catch (e1: NoSuchAlgorithmException) {
            // throw an exception or pick a different encryption method
            throw SignatureException(
                "error building signature, no such algorithm in device "
                        + HASH_ALGORITHM
            )
        } catch (e: InvalidKeyException) {
            throw SignatureException(
                "error building signature, invalid key $HASH_ALGORITHM"
            )
        }
    }

    private fun toHexString(bytes: ByteArray): String? {
        val sb = StringBuilder(bytes.size * 2)
        val formatter = Formatter(sb)
        for (b in bytes) {
            formatter.format("%02x", b)
        }
        return sb.toString()
    }
}