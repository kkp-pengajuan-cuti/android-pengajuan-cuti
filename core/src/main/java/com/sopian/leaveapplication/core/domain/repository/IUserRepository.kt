package com.sopian.leaveapplication.core.domain.repository

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.domain.model.UserHrd
import kotlinx.coroutines.flow.Flow

interface IUserRepository {

    fun register(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>>
    fun getHrd(): Flow<Resource<out List<UserHrd>>>
}