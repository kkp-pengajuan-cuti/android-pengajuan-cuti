package com.sopian.leaveapplication.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Leave(
    val id: Int,
    val subject: String,
    val description: String?,
    val startDate: String,
    val endDate: String,
    val duration: String,
    val employee: String,
    val hrd: String,
    val status: String,
    val type: String,
    val rejectReason: String?,
) : Parcelable
