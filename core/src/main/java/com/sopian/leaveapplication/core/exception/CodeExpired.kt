package com.sopian.leaveapplication.core.exception

import java.lang.Exception

class CodeExpired constructor(message: String?) : Exception(message)