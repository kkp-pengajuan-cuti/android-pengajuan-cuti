package com.sopian.leaveapplication.core.di

import android.content.Context
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.source.remote.AccessTokenInterceptor
import com.sopian.leaveapplication.core.data.source.remote.network.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val LEAVE_API_BASE_URL = "https://2101-180-252-81-39.ngrok.io/api/"

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun provideOkHttpClient(
        accessTokenInterceptor: AccessTokenInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder().apply {
            connectTimeout(120, TimeUnit.SECONDS)
            readTimeout(120, TimeUnit.SECONDS)
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            addInterceptor(accessTokenInterceptor)
        }.build()
    }

    @Provides
    fun provideApiService(client: OkHttpClient): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(LEAVE_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun createAccessTokenInterceptor(
        appLocalData: AppLocalData,
        @ApplicationContext context: Context
    ): AccessTokenInterceptor {
        return AccessTokenInterceptor(appLocalData, context)
    }
}