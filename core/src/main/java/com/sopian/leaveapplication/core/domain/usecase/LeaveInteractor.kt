package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.LeaveRepository
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.domain.model.Leave
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LeaveInteractor @Inject constructor(
    private val leaveRepository: LeaveRepository
) : LeaveUseCase {
    override fun getLeaves(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out List<Leave>>> =
        leaveRepository.getLeaves(hash, payloadRequest)

    override fun approve(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        leaveRepository.approve(hash, payloadRequest)

    override fun reject(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        leaveRepository.reject(hash, payloadRequest)

    override fun createLeave(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        leaveRepository.createLeave(hash, payloadRequest)
}