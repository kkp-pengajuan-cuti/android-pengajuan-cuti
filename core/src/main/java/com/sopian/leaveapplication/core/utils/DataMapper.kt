package com.sopian.leaveapplication.core.utils

import com.sopian.leaveapplication.core.data.source.remote.response.EmptyDataResponse
import com.sopian.leaveapplication.core.data.source.remote.response.LeaveResponse
import com.sopian.leaveapplication.core.data.source.remote.response.TokenResponse
import com.sopian.leaveapplication.core.data.source.remote.response.UserHrdResponse
import com.sopian.leaveapplication.core.domain.model.*

object DataMapper {

    fun mapTokenResponsesToDomain(input: TokenResponse?): Token {
        val user = input?.user?.let {
            User(
                it.nip, it.name, it.email, it.userType,
                it.deviceId, it.profilePhotoUrl
            )
        }
        return Token(
            user = user!!,
            accesToken = input.accesToken,
            refreshToken = input.refreshToken
        )
    }

    fun mapLeaveResponseToDomain(input: List<LeaveResponse>?): List<Leave> {
        val leaveList = ArrayList<Leave>()

        input?.map { leaveResponse ->
            val leave = Leave(
                leaveResponse.id,
                leaveResponse.subject,
                leaveResponse.description,
                leaveResponse.startDate,
                leaveResponse.endDate,
                leaveResponse.duration,
                leaveResponse.employee,
                leaveResponse.hrd,
                leaveResponse.status,
                leaveResponse.type,
                leaveResponse.rejectReason
            )

            leaveList.add(leave)
        }

        return leaveList
    }

    fun mapUserHrdResponseToDomain(input: List<UserHrdResponse>?): List<UserHrd> {
        val hrdList = ArrayList<UserHrd>()

        input?.map {
            val hrd = UserHrd(
                it.id,
                it.name
            )

            hrdList.add(hrd)
        }

        return hrdList
    }

    fun mapEmptyDataResponseToDomain(success: Boolean, message: String): EmptyData {
        return EmptyData(
            success,
            message
        )
    }
}