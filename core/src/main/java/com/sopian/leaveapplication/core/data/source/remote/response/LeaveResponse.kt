package com.sopian.leaveapplication.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class LeaveResponse(
    @field:SerializedName("id")
    val id: Int,

    @field:SerializedName("subject")
    val subject: String,

    @field:SerializedName("description")
    val description: String,

    @field:SerializedName("start_date")
    val startDate: String,

    @field:SerializedName("end_date")
    val endDate: String,

    @field:SerializedName("duration")
    val duration: String,

    @field:SerializedName("employee")
    val employee: String,

    @field:SerializedName("hrd")
    val hrd: String,

    @field:SerializedName("status")
    val status: String,

    @field:SerializedName("type")
    val type: String,

    @field:SerializedName("reject_reason")
    val rejectReason: String,
)
