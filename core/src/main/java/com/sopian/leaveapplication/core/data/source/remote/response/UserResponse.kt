package com.sopian.leaveapplication.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @field:SerializedName("nip")
    val nip: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("email")
    val email: String,

    @field:SerializedName("user_type")
    val userType: String,

    @field:SerializedName("device_id")
    val  deviceId: String,

    @field:SerializedName("profile_photo_url")
    val  profilePhotoUrl: String,
)
