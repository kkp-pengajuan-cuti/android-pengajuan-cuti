package com.sopian.leaveapplication.core.domain.repository

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import kotlinx.coroutines.flow.Flow

interface IVerificationRepository {

    fun verify(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>>
}