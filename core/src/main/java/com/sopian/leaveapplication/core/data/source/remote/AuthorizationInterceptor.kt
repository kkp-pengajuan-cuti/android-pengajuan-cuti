package com.sopian.leaveapplication.core.data.source.remote

import android.content.Context
import com.sopian.leaveapplication.core.AppLocalData
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

private const val UNAUTHORIZED = 401
private const val FORBIDDEN = 403

class AuthorizationInterceptor @Inject constructor(
    private val appLocalData: AppLocalData,
    @ApplicationContext private val context: Context
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        if (response.code == UNAUTHORIZED || response.code == FORBIDDEN){
            appLocalData.dropUserLoggedIn(context)
        }
        return response
    }

}