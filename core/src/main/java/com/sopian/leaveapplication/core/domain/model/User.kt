package com.sopian.leaveapplication.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User (
    val nip: Int,
    val name: String,
    val email: String,
    val userType: String,
    val deviceId: String,
    val profilePhotoUrl: String
) : Parcelable
