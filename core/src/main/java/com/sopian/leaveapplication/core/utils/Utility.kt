package com.sopian.leaveapplication.core.utils

import android.app.AlertDialog
import android.content.Context
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.sopian.leaveapplication.core.R

fun View.show(){
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.INVISIBLE
}

fun View.remove(){
    this.visibility = View.GONE
}

fun String.isValidEmail(): Boolean
        = this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

// Show alert dialog
fun Context.showAlertDialog(positiveButtonLable : String = getString(R.string.ok),
                            title : String = getString(R.string.alert), message : String,
                            actionOnPositveButton : () -> Unit) {
    val builder = AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setCancelable(true)
        .setNegativeButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }
        .setPositiveButton(positiveButtonLable) { dialog, _ ->
            dialog.cancel()
            actionOnPositveButton()
        }
    val alert = builder.create()
    alert?.show()
}

fun Context.showAlertDialogForm(positiveButtonLable : String = getString(R.string.ok),
                                title : String = getString(R.string.alert),
                                message : String,
                                actionOnPositveButton : (String) -> Unit) {

    val inputEditTextField = EditText(this)

    val alert = AlertDialog.Builder(this).apply {
        setTitle(title)
        setMessage(message)
        setView(inputEditTextField)
        setCancelable(true)
        setNegativeButton("Cancel") { dialog, _ ->
            dialog.cancel()
        }
        setPositiveButton(positiveButtonLable) { dialog, _ ->
            dialog.cancel()
            val editTextInput = inputEditTextField.text.toString()
            actionOnPositveButton(editTextInput)
        }
    }.create()
    alert?.show()
}

// Toash extensions
fun Context.showShotToast(message : String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showLongToast(message : String){
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

// Snackbar Extensions
fun View.showShotSnackbar(message : String){
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}

fun View.showLongSnackbar(message : String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun View.hideKeyboard(): Boolean {
    try {
        val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    } catch (ignored: RuntimeException) { }
    return false
}

fun View.showKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.requestFocus()
    imm.showSoftInput(this, 0)
}