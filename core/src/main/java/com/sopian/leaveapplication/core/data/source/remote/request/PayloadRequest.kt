package com.sopian.leaveapplication.core.data.source.remote.request

import com.google.gson.annotations.SerializedName

data class PayloadRequest(

    @field:SerializedName("payload")
    val payload: String,
)
