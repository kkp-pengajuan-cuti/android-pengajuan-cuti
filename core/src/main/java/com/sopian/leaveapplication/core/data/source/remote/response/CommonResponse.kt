package com.sopian.leaveapplication.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class CommonResponse <out T: Any?>(
    @field:SerializedName("success")
    val success: Boolean,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("data")
    val data: T
)