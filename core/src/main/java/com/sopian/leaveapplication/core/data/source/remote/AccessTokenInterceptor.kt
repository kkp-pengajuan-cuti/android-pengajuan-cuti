package com.sopian.leaveapplication.core.data.source.remote

import android.content.Context
import com.sopian.leaveapplication.core.AppLocalData
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

private const val CONTENT_TYPE = "Content-Type"
private const val ACCEPT = "Accept"
private const val APPLICATION_JSON = "application/json"

class AccessTokenInterceptor @Inject constructor(
    private val appLocalData: AppLocalData,
    @ApplicationContext private val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = appLocalData.getKeyAccessToken(context)
        return if (token != null) {
            val authenticatedRequest = chain.request()
                .newBuilder()
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .addHeader(ACCEPT, APPLICATION_JSON)
                .addHeader("Authorization", "Bearer $token")
                .build()
            val response = chain.proceed(authenticatedRequest)
            if (response.code == 401) {
                appLocalData.dropUserLoggedIn(context)
            }
            response
        } else {
            val authenticatedRequest = chain.request()
                .newBuilder()
                .addHeader(CONTENT_TYPE, APPLICATION_JSON)
                .addHeader(ACCEPT, APPLICATION_JSON)
                .build()
            chain.proceed(authenticatedRequest)
        }
    }

}