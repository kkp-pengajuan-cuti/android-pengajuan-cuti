package com.sopian.leaveapplication.core.data.source

import com.sopian.leaveapplication.core.data.NetworkBoundResource
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.RemoteDataSource
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.data.source.remote.network.ApiResponse
import com.sopian.leaveapplication.core.data.source.remote.response.TokenResponse
import com.sopian.leaveapplication.core.domain.model.Token
import com.sopian.leaveapplication.core.domain.repository.ITokenRepository
import com.sopian.leaveapplication.core.utils.DataMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : ITokenRepository{

    override fun getToken(hash: String, tokenRequest: TokenRequest): Flow<Resource<out Token>> =
        object : NetworkBoundResource<Token, TokenResponse>() {
            override fun load(data: TokenResponse?): Flow<Token> {
                return flow {
                    emit(DataMapper.mapTokenResponsesToDomain(data))
                }
            }

            override suspend fun createCall(): Flow<ApiResponse<TokenResponse>> {
                return remoteDataSource.getTokenAndUser(hash, tokenRequest)
            }

        }.asFlow()
}