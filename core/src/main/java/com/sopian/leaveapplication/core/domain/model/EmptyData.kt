package com.sopian.leaveapplication.core.domain.model

data class EmptyData(
    val success: Boolean,
    val message: String
)