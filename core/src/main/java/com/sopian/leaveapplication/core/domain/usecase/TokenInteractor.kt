package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.TokenRepository
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.domain.model.Token
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class TokenInteractor @Inject constructor(
    private val tokenRepository: TokenRepository
) : TokenUseCase {

    override fun getToken(
        hash: String,
        tokenRequest: TokenRequest
    ): Flow<Resource<out Token>> =
        tokenRepository.getToken(hash, tokenRequest)

}