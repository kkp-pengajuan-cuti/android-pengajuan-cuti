package com.sopian.leaveapplication.core.data.source

import com.sopian.leaveapplication.core.data.NetworkBoundResource
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.RemoteDataSource
import com.sopian.leaveapplication.core.data.source.remote.network.ApiResponse
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.data.source.remote.response.LeaveResponse
import com.sopian.leaveapplication.core.data.source.remote.response.UserHrdResponse
import com.sopian.leaveapplication.core.domain.model.Leave
import com.sopian.leaveapplication.core.domain.model.UserHrd
import com.sopian.leaveapplication.core.domain.repository.IUserRepository
import com.sopian.leaveapplication.core.utils.DataMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : IUserRepository{

    override fun register(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>> =
        object : NetworkBoundResource<Unit, Unit>() {

            override suspend fun createCall(): Flow<ApiResponse<Unit>> {
                return remoteDataSource.register(hash, payloadRequest)
            }

            override fun load(data: Unit?): Flow<Unit> {
                return flow { emit(data)  } as Flow<Unit>
            }

        }.asFlow()

    override fun getHrd(): Flow<Resource<out List<UserHrd>>> =
        object : NetworkBoundResource<List<UserHrd>, List<UserHrdResponse>>() {
            override fun load(data: List<UserHrdResponse>?): Flow<List<UserHrd>> {
                return flow {
                    emit(DataMapper.mapUserHrdResponseToDomain(data))
                }
            }

            override suspend fun createCall(): Flow<ApiResponse<List<UserHrdResponse>?>> =
                remoteDataSource.getHrd()

        }.asFlow()
}