package com.sopian.leaveapplication.core.domain.repository

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.domain.model.Leave
import kotlinx.coroutines.flow.Flow

interface ILeaveRepository {

    fun getLeaves(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out List<Leave>>>

    fun approve(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>>

    fun reject(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>>

    fun createLeave(hash: String, payloadRequest: PayloadRequest): Flow<Resource<out Unit>>
}