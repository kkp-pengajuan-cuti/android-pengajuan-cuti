package com.sopian.leaveapplication.core.di

import com.sopian.leaveapplication.core.data.source.TokenRepository
import com.sopian.leaveapplication.core.data.source.UserRepository
import com.sopian.leaveapplication.core.data.source.VerificationRepository
import com.sopian.leaveapplication.core.domain.repository.ITokenRepository
import com.sopian.leaveapplication.core.domain.repository.IUserRepository
import com.sopian.leaveapplication.core.domain.repository.IVerificationRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideTokenRepository(
        tokenRepository: TokenRepository
    ) : ITokenRepository

    @Binds
    abstract fun provideUserRepository(
        userRepository: UserRepository
    ) : IUserRepository

    @Binds
    abstract fun provideVerificationRepository(
        verificationRepository: VerificationRepository
    ) : IVerificationRepository
}