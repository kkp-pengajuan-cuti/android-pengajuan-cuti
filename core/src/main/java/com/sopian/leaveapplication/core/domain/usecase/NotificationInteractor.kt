package com.sopian.leaveapplication.core.domain.usecase

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.NotificationRepository
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import javax.inject.Inject

class NotificationInteractor @Inject constructor(
    private val notificationRepository: NotificationRepository
) : NotificationUseCase {

    override fun registerNotification(hash: String, payloadRequest: PayloadRequest): Resource<out Unit> =
        notificationRepository.registerNotification(hash, payloadRequest)
}