package com.sopian.leaveapplication.core.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Token(
    val user: User,
    val accesToken: String,
    val refreshToken: String
) : Parcelable