package com.sopian.leaveapplication.core.data.source

import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.RemoteDataSource
import com.sopian.leaveapplication.core.data.source.remote.request.PayloadRequest
import com.sopian.leaveapplication.core.domain.repository.INotificationRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : INotificationRepository {

    override fun registerNotification(hash: String, payloadRequest: PayloadRequest): Resource<out Unit> =
        remoteDataSource.registerNotification(hash, payloadRequest)
}