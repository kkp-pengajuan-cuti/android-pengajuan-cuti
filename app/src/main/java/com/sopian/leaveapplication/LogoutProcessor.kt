package com.sopian.leaveapplication

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.sopian.leaveapplication.core.AppLocalData

class LogoutProcessor constructor(
    private val context: Context?,
    private val fragment: Fragment?
) {
    /**
     * Basically just perform logout and data clearance
     */
    fun execute() {

        //Logout from local data
        AppLocalData().dropUserLoggedIn(context)

        goToLoginFragment()
    }


    /**
     * Go to login fragment
     */
    private fun goToLoginFragment() {
        val navController = fragment!!.findNavController()
        val startDestination = navController.graph.startDestination
        val currentDestination = navController.currentDestination?.id
        val navOptions = currentDestination?.let {
            NavOptions.Builder()
                .setPopUpTo(it, true)
                .build()
        }
        navController.navigate(startDestination, null, navOptions)
    }
}