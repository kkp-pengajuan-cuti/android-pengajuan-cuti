package com.sopian.leaveapplication.ui.createleave

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import com.sopian.leaveapplication.R
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.domain.model.UserHrd
import com.sopian.leaveapplication.core.exception.AccessNotAuthorized
import com.sopian.leaveapplication.core.utils.*
import com.sopian.leaveapplication.databinding.FragmentCreateLeaveBinding
import com.sopian.leaveapplication.ui.LeaveAdapter
import com.sopian.leaveapplication.ui.home.LeaveAwaitingFragment
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class CreateLeaveFragment : Fragment() {

    private var _binding: FragmentCreateLeaveBinding? = null
    private val createLeaveViewModel: CreateLeaveViewModel by viewModels()
    private var listDataUserHrd = ArrayList<UserHrd>()
    private var listDataHrd = ArrayList<String>()

    private var sha: Sha? = null
    private var loggedInUser: LoggedInUser? = null
    private var appLocalData: AppLocalData? = null
    private var deviceId: String? = null

    private var idHrd: Int? = null
    private var typeId: Int? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateLeaveBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sha = Sha()
        appLocalData = AppLocalData()
        loggedInUser = appLocalData?.getUserLoggedIn(context)
        deviceId = appLocalData?.getKeyDeviceId(context)

        createLeaveViewModel.hrd.observe(viewLifecycleOwner, { hrd ->
            when (hrd) {
                is Resource.Success -> {
                    setDataHrd(hrd.data)

                    listDataUserHrd.map {
                        listDataHrd.add(it.name)
                    }
                }
                is Resource.Error -> {
                    Timber.tag("CreateLeaveFragment").e(hrd.message.toString())
                }
            }
        })

        binding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }

        return root
    }

    fun setDataHrd(newListData: List<UserHrd>?) {
        if (newListData == null) return
        listDataUserHrd.clear()
        listDataUserHrd.addAll(newListData)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(data: CreateLeave): String? {
        val payload = JSONObject().apply {
            put("nip", loggedInUser?.nip)
            put("device_id", deviceId)
            put("subject", data.subject)
            put("description", data.description)
            put("start_date", data.startDate)
            put("end_date", data.endDate)
            put("request_to", data.requestTo)
            put("type_id", data.typeId)
        }

        return sha?.hashEncrypt(payload.toString())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val typeData = resources.getStringArray(R.array.type)

        with(binding) {
            val arrayAdapterType =
                ArrayAdapter(requireContext(), R.layout.type_dropdown_item, typeData)
            type.setAdapter(arrayAdapterType)
            type.setOnItemClickListener { _, _, i, _ ->
                typeId = when (typeData[i]) {
                    "Sick Leave" -> 1
                    "Casual Leave" -> 2
                    else -> 3
                }
            }

            val arrayAdapter =
                ArrayAdapter(requireContext(), R.layout.type_dropdown_item, listDataHrd)
            hrd.setAdapter(arrayAdapter)
            hrd.setOnItemClickListener { adapterView, view, i, l ->
                val hrd = listDataUserHrd.get(i)
                idHrd = hrd.id
            }


            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            calendar.clear()

            val today: Long = MaterialDatePicker.todayInUtcMilliseconds()

            startDate.setOnClickListener {
                val startDatePicker = MaterialDatePicker.Builder.datePicker().apply {
                    setTitleText("Select date")
                    setSelection(today)
                }.build()
                startDatePicker.show(parentFragmentManager, "START_DATE")
                startDatePicker.addOnPositiveButtonClickListener {
                    startDate.text = startDatePicker.headerText
                }
            }

            endDate.setOnClickListener {
                val endDatePicker = MaterialDatePicker.Builder.datePicker().apply {
                    setTitleText("Select date")
                    setSelection(today)
                }.build()
                endDatePicker.show(parentFragmentManager, "END_DATE")
                endDatePicker.addOnPositiveButtonClickListener {
                    endDate.text = endDatePicker.headerText
                }
            }



            submit.setOnClickListener {
                val data = CreateLeave(
                    subject = cause.text?.trim().toString(),
                    description = description.text?.trim().toString(),
                    startDate = getFormatDate(startDate.text?.trim().toString())!!,
                    endDate = getFormatDate(endDate.text?.trim().toString())!!,
                    requestTo = idHrd!!,
                    typeId = typeId!!
                )

                val payload = getPayload(data)

                payload?.let {
                    createLeaveViewModel.onSubmitButtonClicked(it)
                }
            }

            createLeaveViewModel.submit.observe(viewLifecycleOwner, {
                when(it) {
                    is Resource.Success -> {
                        context?.showLongToast("Success")
                        val action = CreateLeaveFragmentDirections.actionNavigationCreateLeaveToNavigationHome()
                        findNavController().navigate(action)
                    }
                    is Resource.Error -> {
                        if (it.exception is AccessNotAuthorized) {
                            context?.showLongToast("Unauthenticated")
                        }
                        Timber.tag(LeaveAwaitingFragment.TAG).e(it.message.toString())
                    }
                }
            })
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getFormatDate(date: String): String? {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val simpleDateParseFormat = SimpleDateFormat("MMM d, yyyy")
        try {
            return simpleDateFormat.format(simpleDateParseFormat.parse(date))
        } catch (e: ParseException) {
            Timber.tag(LeaveAdapter.TAG).e("Error while parsing Delivered Date: %s", e.message)
        }

        return null
    }

    data class CreateLeave(
        val subject: String,
        val description: String,
        val startDate: String,
        val endDate: String,
        val requestTo: Int,
        val typeId: Int
    )

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        sha = null
        loggedInUser = null
        appLocalData = null
        deviceId = null
        idHrd = null
        typeId = null
    }

}