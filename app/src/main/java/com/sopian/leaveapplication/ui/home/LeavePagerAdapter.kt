package com.sopian.leaveapplication.ui.home

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

const val LEAVE_AWAITING_PAGE_INDEX = 0
const val LEAVE_DECLINED_PAGE_INDEX = 1
const val LEAVE_APPROVED_PAGE_INDEX = 2

class LeavePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    /**
     * Mapping of the ViewPager page indexes to their respective Fragments
     */
    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        LEAVE_AWAITING_PAGE_INDEX to { LeaveAwaitingFragment() },
        LEAVE_DECLINED_PAGE_INDEX to { LeaveDeclinedFragment() },
        LEAVE_APPROVED_PAGE_INDEX to { LeaveApprovedFragment() }
    )

    override fun getItemCount(): Int = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment {
        return tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
    }
}