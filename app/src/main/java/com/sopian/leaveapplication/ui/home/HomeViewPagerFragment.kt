package com.sopian.leaveapplication.ui.home

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.sopian.leaveapplication.LogoutProcessor
import com.sopian.leaveapplication.R
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.domain.usecase.NotificationUseCase
import com.sopian.leaveapplication.core.utils.Sha
import com.sopian.leaveapplication.core.utils.hide
import com.sopian.leaveapplication.databinding.FragmentHomeViewPagerBinding
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class HomeViewPagerFragment : Fragment() {

    private var _binding: FragmentHomeViewPagerBinding? = null

    @Inject lateinit var notificationUseCase: NotificationUseCase

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var logoutProcessor:LogoutProcessor? = null
    private var loggedInUser: LoggedInUser? = null
    private var appLocalData: AppLocalData? = null
    private var sha: Sha? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeViewPagerBinding.inflate(inflater, container, false)
        val root: View = binding.root

        logoutProcessor = LogoutProcessor(context, this)

        appLocalData = AppLocalData()

        loggedInUser = appLocalData?.getUserLoggedIn(context)

        if(loggedInUser == null){
            logoutProcessor?.execute()
        }

        registerPushNotification()
        createNotificationChannel()

        if (loggedInUser?.userType == "hrd") {
            binding.createLeave.hide()
        }

        binding.name.text = loggedInUser?.displayName

        val tabLayout = binding.tabs
        val viewPager = binding.viewPager

        viewPager.adapter = LeavePagerAdapter(this)

        // Set text for each tab
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = getTabTitle(position)
        }.attach()

        binding.createLeave.setOnClickListener {
            findNavController().navigate(HomeViewPagerFragmentDirections.actionNavigationHomeToNavigationCreateLeave())
        }

        return root
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            LEAVE_AWAITING_PAGE_INDEX -> getString(R.string.leave_awating_title)
            LEAVE_DECLINED_PAGE_INDEX -> getString(R.string.leave_declined_title)
            LEAVE_APPROVED_PAGE_INDEX -> getString(R.string.leave_approved_title)
            else -> null
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            val channelId = getString(R.string.app_chanel)
            val channelName = getString(R.string.channel_name)
            val notificationManager = context?.getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(
                NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_LOW)
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun registerPushNotification(){
        Firebase.messaging.token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Timber.tag(TAG).w( "Fetching FCM registration token failed : ${task.exception}")
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            appLocalData?.setKeyFcmToken(context, token)

            val deviceId = appLocalData?.getKeyDeviceId(context)
            val loggedInUser = AppLocalData().getUserLoggedIn(context)

            if (loggedInUser != null) {
                sendRegistrationToServer(token, deviceId, loggedInUser)
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun sendRegistrationToServer(token: String?, deviceId: String?,
                                         loggedInUser: LoggedInUser): Resource<out Unit> {

        val payload = getPayload(token, loggedInUser, deviceId)
        return notificationUseCase.registerNotification(payload)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(token: String?, loggedInUser: LoggedInUser, deviceId: String?): String {
        val payload = JSONObject().apply {
            put("nip", loggedInUser.nip)
            put("fcm_token", token)
            put("device_id", deviceId)
        }


        return Sha().hashEncrypt(payload.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        logoutProcessor = null
        loggedInUser = null
        appLocalData = null
        sha = null
    }

    companion object {
        const val TAG = "HomeFragment"
    }
}