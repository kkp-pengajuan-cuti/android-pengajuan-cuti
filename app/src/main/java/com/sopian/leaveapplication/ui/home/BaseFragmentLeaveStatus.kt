package com.sopian.leaveapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.utils.Sha
import com.sopian.leaveapplication.core.utils.remove
import com.sopian.leaveapplication.core.utils.show
import com.sopian.leaveapplication.databinding.FragmentLeaveStatusBinding
import com.sopian.leaveapplication.ui.LeaveAdapter
import timber.log.Timber

abstract class BaseFragmentLeaveStatus : Fragment() {

    private val homeViewModel: HomeViewModel by viewModels()
    private var _binding: FragmentLeaveStatusBinding? = null
    private val binding get() = _binding!!

    private var sha: Sha? = null
    private var leaveAdapter: LeaveAdapter? = null
    private var loggedInUser: LoggedInUser? = null
    private var appLocalData: AppLocalData? = null
    private var deviceId: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLeaveStatusBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sha = Sha()
        leaveAdapter = context?.let { LeaveAdapter(it) }
        appLocalData = AppLocalData()
        loggedInUser = appLocalData?.getUserLoggedIn(context)
        deviceId = appLocalData?.getKeyDeviceId(context)

        val payload = getPayload()
        if (payload != null) {
            homeViewModel.onLeaveLoaded(payload)
        }

        return root
    }

    abstract fun getPayload(): String?

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeUi()

        with(binding.rvLeave) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = leaveAdapter
        }
    }

    private fun subscribeUi() {
        homeViewModel.leave.observe(viewLifecycleOwner, { leave ->
            when(leave) {
                is Resource.Loading -> binding.progressBar.show()
                is Resource.Success -> {
                    binding.progressBar.remove()
                    leaveAdapter?.setData(leave.data)
                }
                is Resource.Error -> {
                    Timber.tag(TAG).e(leave.message.toString())
                    binding.progressBar.remove()
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        sha = null
        leaveAdapter = null
    }

    companion object {
        const val TAG = "BaseFragmentLeaveStatus"
    }
}