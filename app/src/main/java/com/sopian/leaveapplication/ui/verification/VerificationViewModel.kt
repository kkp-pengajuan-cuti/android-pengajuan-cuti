package com.sopian.leaveapplication.ui.verification

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import com.sopian.leaveapplication.core.domain.usecase.VerificationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VerificationViewModel @Inject constructor(
    private val verificationUseCase: VerificationUseCase
) : ViewModel(){

    private val _payload = MutableLiveData<String>()

    fun onVerifyClicked(payload: String) {
        _payload.value = payload
    }

    val verify = _payload.switchMap {
        verificationUseCase.verify(it).asLiveData()
    }
}