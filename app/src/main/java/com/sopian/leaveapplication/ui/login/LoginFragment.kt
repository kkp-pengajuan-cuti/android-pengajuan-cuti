package com.sopian.leaveapplication.ui.login

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.sopian.leaveapplication.BuildConfig
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.utils.*
import com.sopian.leaveapplication.databinding.FragmentLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import timber.log.Timber

/**
 * Fragment that handle login process
 */
@AndroidEntryPoint
class LoginFragment : Fragment() {

    private val loginViewModel: LoginViewModel by viewModels()

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var appLocalData: AppLocalData? = null

    private var deviceId: String? = null

    private var sha: Sha? = null

    companion object {
        const val TAG = "LoginFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        appLocalData = AppLocalData()
        sha = Sha()

        if (appLocalData?.getKeyDeviceId(context) == null) {
            appLocalData?.setKeyDeviceId(context, appLocalData?.getGeneratedDeviceId())
        }

        deviceId = appLocalData?.getKeyDeviceId(context)

        val json = JSONObject()
        json.put("nip", 1811511490)

        val payload = json.toString()

        val str = payload + Sha.KEY

        println("sha test : ${sha!!.hashMac(str, Sha.KEY)}")

        //Check if current user already logged in
        val loggedInUser: LoggedInUser? = appLocalData?.getUserLoggedIn(context)

        // if user logged in, it will redirect to home
        if (loggedInUser != null) {
            val action = LoginFragmentDirections.actionNavigationLoginToNavigationHome()
            findNavController().navigate(action)
        }

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.register.setOnClickListener {
            findNavController().navigate(
                LoginFragmentDirections.actionNavigationLoginToNavigationRegister()
            )
        }

        binding.login.setOnClickListener {

            val params = TokenRequest(
                "password",
                Integer.parseInt(BuildConfig.CLIENT_ID),
                BuildConfig.CLIENT_SECRET,
                binding.username.text.toString().trim(),
                binding.password.text.toString().trim(),
                "*"
            )

            Timber.tag(TAG).d(params.toString())

            val payload = getPayload(binding)
            Timber.tag(TAG).d(payload)
            if (payload != null) {
                loginViewModel.getToken(payload, params).observe(viewLifecycleOwner, { token ->
                    if (token != null){
                        when (token) {
                            is Resource.Loading -> binding.llProgressBar.root.show()
                            is Resource.Success -> {
                                binding.llProgressBar.root.remove()
                                val action = LoginFragmentDirections.actionNavigationLoginToNavigationVerificationCode(token.data)
                                findNavController().navigate(action)
                            }
                            is Resource.Error -> {
                                binding.llProgressBar.root.remove()
                                context?.showLongToast("Login failed")
                            }
                        }
                    }
                })
            } else {
                Timber.tag(TAG).e("Can't get payload data")
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(binding: FragmentLoginBinding): String? {
        val json = JSONObject()
        json.put("nip", Integer.parseInt(binding.username.text.toString()))
        json.put("device_id", deviceId)

        return sha?.hashEncrypt(json.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        appLocalData = null
        deviceId = null
        sha = null
    }
}