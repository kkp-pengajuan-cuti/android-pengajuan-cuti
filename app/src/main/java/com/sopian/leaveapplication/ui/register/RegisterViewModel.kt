package com.sopian.leaveapplication.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import com.sopian.leaveapplication.core.domain.usecase.NotificationUseCase
import com.sopian.leaveapplication.core.domain.usecase.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val userUseCase: UserUseCase
): ViewModel() {

    private val _register = MutableLiveData<String>()

    fun onRegisterClicked(payload: String) {
        _register.value = payload
    }

    val register = _register.switchMap {
        userUseCase.register(it).asLiveData()
    }
}