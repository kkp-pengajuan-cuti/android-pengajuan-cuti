package com.sopian.leaveapplication.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import com.sopian.leaveapplication.core.domain.model.Leave
import com.sopian.leaveapplication.core.domain.usecase.LeaveUseCase
import com.sopian.leaveapplication.core.utils.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val leaveUseCase: LeaveUseCase
) : ViewModel() {

    private val _leave = MutableLiveData<String>()

    fun onLeaveLoaded(payload: String) {
        _leave.value = payload
    }

    val leave = _leave.switchMap {
        leaveUseCase.getLeaves(it).asLiveData()
    }

    private val _approve = MutableLiveData<String>()
    private val _reject = MutableLiveData<String>()

    fun onApproveButtonClicked(payload: String) {
        _approve.value = payload
    }

    val approve = _approve.switchMap {
        leaveUseCase.approve(it).asLiveData()
    }

    fun onRejectButtonClicked(payload: String) {
        _reject.value = payload
    }

    val reject = _reject.switchMap {
        leaveUseCase.reject(it).asLiveData()
    }
}