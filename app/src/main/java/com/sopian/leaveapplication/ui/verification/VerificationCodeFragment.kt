package com.sopian.leaveapplication.ui.verification

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.domain.model.Token
import com.sopian.leaveapplication.core.exception.CodeExpired
import com.sopian.leaveapplication.core.exception.CodeNotMatch
import com.sopian.leaveapplication.core.utils.*
import com.sopian.leaveapplication.databinding.FragmentVerificationCodeBinding
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject

@AndroidEntryPoint
class VerificationCodeFragment : Fragment() {

    private val args: VerificationCodeFragmentArgs by navArgs()

    private val verificationViewModel: VerificationViewModel by viewModels()

    private var _binding: FragmentVerificationCodeBinding? = null
    private val binding get() = _binding!!

    private var appLocalData:AppLocalData? = null
    private var sha: Sha? = null

    companion object {
        const val TAG = "VerificationCodeFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentVerificationCodeBinding.inflate(inflater, container, false)
        sha = Sha()
        appLocalData = AppLocalData()
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.apply {
            verifyButton.setOnClickListener { _ ->
                val data = args.token
                val payload = data?.let { getPayload(this, it) }
                if (payload != null) {
                    verificationViewModel.onVerifyClicked(payload)
                }
            }

            verificationViewModel.verify.observe(viewLifecycleOwner, {
                when(it) {
                    is Resource.Loading -> llProgressBar.root.show()
                    is Resource.Success -> {
                        llProgressBar.root.remove()
                        processToHomeFragment()
                    }
                    is Resource.Error -> {
                        when(it.exception) {
                            is CodeNotMatch -> context?.showAlertDialog(
                                message = "The verification code you have entered is invalid. " +
                                        "Please enter a valid verification code"
                            ){}
                            is CodeExpired -> context?.showAlertDialog(
                                message = "The verification code you have entered is expired. " +
                                        "Please resend code and enter a valid verification code"
                            ){}
                        }
                        llProgressBar.root.remove()
                    }
                }
            })
        }
    }

    private fun processToHomeFragment() {
        val data = args.token
        var user: LoggedInUser? = null
        data?.let {
            user = LoggedInUser(
                it.user.nip,
                it.user.name,
                it.accesToken,
                it.refreshToken,
                it.user.profilePhotoUrl,
                it.user.userType
            )
        }

        user?.let { appLocalData?.setUserLoggedIn(context, it) }

        val action = VerificationCodeFragmentDirections
            .actionNavigationVerificationCodeToNavigationHome()

        findNavController().navigate(action)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(binding: FragmentVerificationCodeBinding, params: Token) : String?{

        val deviceId = appLocalData?.getKeyDeviceId(context)

        val payload = JSONObject()
        payload.apply {
            put("nip", params.user.nip)
            put("code", Integer.parseInt(binding.code.text.toString().trim()))
            put("device_id", deviceId)
            put("is_register", 0)
        }

        return sha?.hashEncrypt(payload.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        appLocalData = null
        sha = null
    }

}