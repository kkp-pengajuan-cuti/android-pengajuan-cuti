package com.sopian.leaveapplication.ui.createleave

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import com.sopian.leaveapplication.core.domain.usecase.LeaveUseCase
import com.sopian.leaveapplication.core.domain.usecase.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreateLeaveViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
    private val leaveUseCase: LeaveUseCase
): ViewModel() {

    val hrd = userUseCase.getHrd().asLiveData()

    private val _submit = MutableLiveData<String>()

    fun onSubmitButtonClicked(payload: String) {
        _submit.value = payload
    }

    val submit = _submit.switchMap {
        leaveUseCase.createLeave(it).asLiveData()
    }
}