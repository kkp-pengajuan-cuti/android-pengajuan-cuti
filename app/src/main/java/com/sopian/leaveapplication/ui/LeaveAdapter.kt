package com.sopian.leaveapplication.ui

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.sopian.leaveapplication.R
import com.sopian.leaveapplication.core.domain.model.Leave
import com.sopian.leaveapplication.databinding.ItemListLeaveBinding

import android.os.Build
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.utils.remove
import com.sopian.leaveapplication.core.utils.show
import timber.log.Timber
import java.lang.NullPointerException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class LeaveAdapter(private val context: Context) : RecyclerView.Adapter<LeaveAdapter.ListViewHolder>() {

    private var listData = ArrayList<Leave>()
    var onApproveButtonClick: ((Leave) -> Unit)? = null
    var onRejectButtonClick: ((Leave) -> Unit)? = null

    fun setData(newListData: List<Leave>?) {
        if (newListData == null) return
        listData.clear()
        listData.addAll(newListData)
        notifyDataSetChanged()
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemListLeaveBinding.bind(itemView)
        val loggedInUser = AppLocalData().getUserLoggedIn(context)
        fun bind(data: Leave) {
            with(binding) {
                when(data.status) {
                    "1" -> {
                        status.text = context.getString(R.string.leave_awating_title)
                        setBackgroundTintLabelStatus(binding, R.drawable.rounded_corner_awaiting_status)
                        setTextColorLabelStatus(binding, R.color.amber_400)
                        when(loggedInUser?.userType) {
                            "karyawan" -> {
                                btnApprove.remove()
                                btnReject.remove()
                            }
                            else -> {
                                btnReject.show()
                                btnApprove.show()
                            }
                        }
                    }
                    "2" -> {
                        status.text = context.getString(R.string.leave_declined_title)
                        if (data.rejectReason != null) {
                            rejectLabel.show()
                            rejectReason.show()
                            rejectReason.text = data.rejectReason
                        }
                        setBackgroundTintLabelStatus(binding, R.drawable.rounded_corner_declined_status)
                        setTextColorLabelStatus(binding, R.color.red_200)
                        btnApprove.remove()
                        btnReject.remove()
                    }
                    else -> {
                        status.text = context.getString(R.string.leave_approved_title)
                        setBackgroundTintLabelStatus(binding, R.drawable.rounded_corner_approved_status)
                        setTextColorLabelStatus(binding, R.color.green_200)
                        btnApprove.remove()
                        btnReject.remove()
                    }
                }

                subject.text = data.subject
                description.text = "\" ${data.description} \""
                typeLeave.text = data.type

                val sd = data.startDate
                val ed = data.endDate
                startEnd.text = String.format(context.getString(R.string.card_start_end_date), getFormatDate(sd), getFormatDate(ed))


                if (loggedInUser?.userType == "karyawan") {
                    applicant.text = "HRD : "
                    applicantValue.text = data.hrd
                }else{
                    applicant.text = "Employee : "
                    applicantValue.text = data.employee
                }

            }
        }

        init {
            with(binding){
                btnApprove.setOnClickListener {
                    onApproveButtonClick?.invoke(listData[adapterPosition])
                }

                btnReject.setOnClickListener {
                    onRejectButtonClick?.invoke(listData[adapterPosition])
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getFormatDate(date: String): String? {
        val simpleDateFormat = SimpleDateFormat("EEE, MMM d, ''yy")
        val simpleDateParseFormat = SimpleDateFormat("yyyy-MM-dd")
        try {
            return simpleDateFormat.format(simpleDateParseFormat.parse(date))
        } catch (e: ParseException) {
            Timber.tag(TAG).e("Error while parsing Delivered Date: %s", e.message)
        }

        return null
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder =
        ListViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_leave, parent, false))

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val data = listData[position]
        holder.bind(data)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setBackgroundTintLabelStatus(binding: ItemListLeaveBinding, @DrawableRes id: Int){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.status.background = context.getDrawable(id)
        }else{
            binding.status.background = context.resources.getDrawable(id)
        }
    }

    private fun setTextColorLabelStatus(binding: ItemListLeaveBinding, @ColorRes idColor: Int) {
        try {
            binding.status.setTextColor(
                ContextCompat.getColor(
                    context,
                    idColor
                )
            )
        } catch (e: NullPointerException) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                binding.status.setTextColor(context.getColor(idColor))
            } else {
                binding.status.setTextColor(context.resources.getColor(idColor))
            }
        }
    }

    override fun getItemCount(): Int = listData.size

    companion object {
        const val TAG = "LeaveAdapter"
    }
}