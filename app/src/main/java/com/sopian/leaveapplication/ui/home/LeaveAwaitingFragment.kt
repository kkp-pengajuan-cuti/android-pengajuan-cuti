package com.sopian.leaveapplication.ui.home

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.sopian.leaveapplication.LogoutProcessor
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.domain.model.Leave
import com.sopian.leaveapplication.core.domain.model.LoggedInUser
import com.sopian.leaveapplication.core.exception.AccessNotAuthorized
import com.sopian.leaveapplication.core.utils.*
import com.sopian.leaveapplication.databinding.FragmentLeaveStatusBinding
import com.sopian.leaveapplication.ui.LeaveAdapter
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import timber.log.Timber

@AndroidEntryPoint
class LeaveAwaitingFragment : Fragment() {
    private val homeViewModel: HomeViewModel by viewModels()
    private var _binding: FragmentLeaveStatusBinding? = null
    private val binding get() = _binding!!

    private var sha: Sha? = null
    private var leaveAdapter: LeaveAdapter? = null
    private var loggedInUser: LoggedInUser? = null
    private var appLocalData: AppLocalData? = null
    private var logoutProcessor: LogoutProcessor? = null
    private var deviceId: String? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLeaveStatusBinding.inflate(inflater, container, false)
        val root: View = binding.root

        logoutProcessor = LogoutProcessor(context, this)

        sha = Sha()
        leaveAdapter = context?.let { LeaveAdapter(it) }
        appLocalData = AppLocalData()
        loggedInUser = appLocalData?.getUserLoggedIn(context)
        deviceId = appLocalData?.getKeyDeviceId(context)

        val payload = getPayload()
        if (payload != null) {
            homeViewModel.onLeaveLoaded(payload)
        }

        return root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(leave: Leave? = null, reason: String? = null): String? {

        val payload = if (leave != null) {
            JSONObject().apply {
                put("id", leave.id)
                put("nip", loggedInUser?.nip)
                put("device_id", deviceId)
                put("user_type", loggedInUser?.userType)
                put("reason", reason)
            }

        } else {
            JSONObject().apply {
                put("nip", loggedInUser?.nip)
                put("device_id", deviceId)
                put("status", 1)
                put("user_type", loggedInUser?.userType)
            }

        }
        return sha?.hashEncrypt(payload.toString())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        leaveAdapter?.onApproveButtonClick = {
            context?.showAlertDialog(title = "Alert", message = "Are you sure?"){
                val payload = getPayload(it)
                payload?.let { homeViewModel.onApproveButtonClicked(payload) }
            }
        }

        leaveAdapter?.onRejectButtonClick = {
            context?.showAlertDialogForm(title = "Alert", message = "Are you sure?"){ reason ->
                val payload = getPayload(it, reason)
                payload?.let { homeViewModel.onRejectButtonClicked(payload) }
            }
        }

        val payload = getPayload()

        subscribeUi()
        subscribeAction(payload)

        with(binding.rvLeave) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = leaveAdapter
        }
    }

    private fun subscribeAction(payload: String? = null) {
        with(homeViewModel) {
            approve.observe(viewLifecycleOwner, {
                when(it) {
                    is Resource.Loading -> binding.progressBar.show()
                    is Resource.Success -> {
                        context?.showLongToast("Success")

                        parentFragmentManager.beginTransaction()
                            .detach(this@LeaveAwaitingFragment)
                            .attach(this@LeaveAwaitingFragment)
                            .commit()

                        binding.progressBar.remove()
                        payload?.let { payload -> homeViewModel.onLeaveLoaded(payload) }
                    }
                    is Resource.Error -> {
                        if (it.exception is AccessNotAuthorized) {
                            context?.showLongToast("Unauthenticated")
                        }
                        Timber.tag(TAG).e(it.message.toString())
                        binding.progressBar.remove()
                    }
                }
            })

            reject.observe(viewLifecycleOwner, {
                when(it) {
                    is Resource.Loading -> binding.progressBar.show()
                    is Resource.Success -> {
                        context?.showLongToast("Success")

                        parentFragmentManager.beginTransaction()
                            .detach(this@LeaveAwaitingFragment)
                            .attach(this@LeaveAwaitingFragment)
                            .commit()

                        binding.progressBar.remove()
                        payload?.let { payload -> homeViewModel.onLeaveLoaded(payload) }
                    }
                    is Resource.Error -> {
                        if (it.exception is AccessNotAuthorized) {
                            context?.showLongToast("Unauthenticated")
                        }
                        Timber.tag(TAG).e(it.message.toString())
                        binding.progressBar.remove()
                    }
                }
            })
        }
    }

    private fun subscribeUi() {
        with(homeViewModel){
            leave.observe(viewLifecycleOwner, { leave ->
                when(leave) {
                    is Resource.Loading -> binding.progressBar.show()
                    is Resource.Success -> {
                        binding.progressBar.remove()
                        leaveAdapter?.setData(leave.data)
                    }
                    is Resource.Error -> {
                        if (leave.exception is AccessNotAuthorized) {
                            logoutProcessor?.execute()
                        }
                        Timber.tag(TAG).e(leave.message.toString())
                        binding.progressBar.remove()
                    }
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        sha = null
        leaveAdapter = null
    }

    companion object {
        const val TAG = "LeaveAwaitingFragment"
    }
}