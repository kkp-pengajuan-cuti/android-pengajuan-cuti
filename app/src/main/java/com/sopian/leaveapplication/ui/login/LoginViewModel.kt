package com.sopian.leaveapplication.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.domain.model.Token
import com.sopian.leaveapplication.core.domain.usecase.TokenUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val tokenUseCase: TokenUseCase) : ViewModel() {

    fun getToken(payload: String, tokenRequest: TokenRequest): LiveData<Resource<out Token>> {
        return tokenUseCase.getToken(payload, tokenRequest).asLiveData()
    }
}