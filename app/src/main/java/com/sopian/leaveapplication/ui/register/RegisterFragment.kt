package com.sopian.leaveapplication.ui.register

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.sopian.leaveapplication.BuildConfig
import com.sopian.leaveapplication.R
import com.sopian.leaveapplication.core.AppLocalData
import com.sopian.leaveapplication.core.data.Resource
import com.sopian.leaveapplication.core.data.source.remote.request.TokenRequest
import com.sopian.leaveapplication.core.exception.NipNotFound
import com.sopian.leaveapplication.core.utils.*
import com.sopian.leaveapplication.databinding.FragmentRegisterBinding
import com.sopian.leaveapplication.ui.login.LoginFragment
import com.sopian.leaveapplication.ui.login.LoginViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import timber.log.Timber

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private val registerViewModel: RegisterViewModel by viewModels()
    private val loginViewModel: LoginViewModel by viewModels()

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var appLocalData: AppLocalData? = null

    private var payload: String? = null
    private var userType = "karyawan"

    private var deviceId: String? = null

    private var params: TokenRequest? = null

    private var sha: Sha? = null

    companion object {
        const val TAG = "RegisterFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        appLocalData = AppLocalData()
        sha = Sha()
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.register.setOnClickListener {
            deviceId = appLocalData?.getGeneratedDeviceId()

            payload = getPayload(binding, deviceId!!)
            Timber.d("payload : $payload")

            params = TokenRequest(
                "password",
                Integer.parseInt(BuildConfig.CLIENT_ID),
                BuildConfig.CLIENT_SECRET,
                binding.nip.text.toString().trim(),
                binding.password.text.toString().trim(),
                "*"
            )
            Timber.tag(LoginFragment.TAG).d(params.toString())

            registerViewModel.onRegisterClicked(payload!!)
        }

        observeEvent()
    }

    private fun observeEvent() {
        registerViewModel.register.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> binding.progressBar.show()
                is Resource.Success -> {

                    loginViewModel.getToken(payload!!, params!!)
                        .observe(viewLifecycleOwner, { token ->
                            if (token != null) {
                                when (token) {
                                    is Resource.Success -> {

                                        appLocalData?.setKeyDeviceId(context, deviceId)

                                        binding.progressBar.remove()
                                        val action =
                                            RegisterFragmentDirections.actionNavigationRegisterToNavigationVerificationCode(
                                                token.data
                                            )
                                        findNavController().navigate(action)
                                    }

                                    is Resource.Error -> {
                                        binding.progressBar.remove()
                                        Timber.tag(TAG).e(token.message.toString())
                                        context?.showLongToast("Register failed")
                                    }
                                }
                            }
                        })
                }
                is Resource.Error -> {
                    binding.progressBar.remove()
                    Timber.tag(TAG).e(it.message.toString())
                    if (it.exception is NipNotFound) {
                        context?.showLongToast("Nip not found")
                    } else {
                        context?.showLongToast(it.message.toString())
                    }
                }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getPayload(binding: FragmentRegisterBinding, deviceId: String): String? {
        val payload = JSONObject()
        payload.apply {
            put("nip", Integer.parseInt(binding.nip.text.toString()))
            put("email", binding.email.text.toString())
            put("password", binding.password.text.toString())
            put("device_id", deviceId)
        }
        return sha?.hashEncrypt(payload.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        appLocalData = null
        sha = null
    }
}