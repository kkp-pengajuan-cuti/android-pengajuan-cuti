package com.sopian.leaveapplication.di

import com.sopian.leaveapplication.core.domain.usecase.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class AppModule {

    @Binds
    @ViewModelScoped
    abstract fun provideTokenUseCase(tokenInterceptor: TokenInteractor): TokenUseCase

    @Binds
    @ViewModelScoped
    abstract fun provideUserUseCase(userInteractor: UserInteractor): UserUseCase

    @Binds
    @ViewModelScoped
    abstract fun provideVerificationUseCase(verificationInteractor: VerificationInteractor) :
            VerificationUseCase

    @Binds
    @ViewModelScoped
    abstract fun provideLeaveUseCase(leaveInteractor: LeaveInteractor) :
            LeaveUseCase
}