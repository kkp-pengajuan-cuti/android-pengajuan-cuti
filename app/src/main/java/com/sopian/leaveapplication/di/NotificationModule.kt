package com.sopian.leaveapplication.di

import com.sopian.leaveapplication.core.domain.usecase.NotificationInteractor
import com.sopian.leaveapplication.core.domain.usecase.NotificationUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NotificationModule {

    @Binds
    abstract fun provideNotificationUseCase(notificationInteractor: NotificationInteractor) :
            NotificationUseCase
}